variable ports {
  description = "map of ports & protocols to be added as listeners to this application loadbalancer. defaults to HTTP listener on port 8080"
  type        = "map"

  default = {
    "8080" = "HTTP"
  }
}

variable protocol {
  default = "HTTP"
}

variable aws_region {
  description = "id of region to create resources in; default is eu-central-1 (Frankfurt)"
}


variable resource_default_tags {
  description = "map: tags that will be set on all resources that support tagging"
  type        = "map"

  default = {
    module = "application-loadblancer"
  }
}

variable security_group_ids {
  description = "list of security groups to be attached to this load balancer. no default provided"
  type        = "list"
}

variable subnet_ids {
  description = "list of IDs of subnets to be served by this load balancer. no default provided"
  type        = "list"
}

variable vpc_id {
  description = "ID of the VPC in which this loadbalancer is placed. no default provided"
  type        = "string"
}
