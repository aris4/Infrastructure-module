resource "aws_alb" "application_loadbalancer" {
  security_groups = ["${var.security_group_ids}"]
  subnets         = ["${var.subnet_ids}"]
  tags            = "${var.resource_default_tags}"
}

resource "aws_alb_listener" "alb_listener" {
  count             = "${length(var.ports)}"
  load_balancer_arn = "${aws_alb.application_loadbalancer.arn}"
  port              = "${element(keys(var.ports), count.index)}"
  protocol          = "${var.ports["${element(keys(var.ports), count.index)}"]}"

  "default_action" {
    target_group_arn = "${element(aws_alb_target_group.alb_target_group.*.arn, count.index)}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "alb_target_group" {
  count    = "${length(var.ports)}"
  port     = "${element(keys(var.ports), count.index)}"
  protocol = "${var.ports["${element(keys(var.ports), count.index)}"]}"
  vpc_id   = "${var.vpc_id}"
  tags     = "${var.resource_default_tags}"
}
