output "alb_arn" {
  value = "${aws_alb.application_loadbalancer.arn}"
}

output "alb_id" {
  value = "${aws_alb.application_loadbalancer.id}"
}

output "alb_name" {
  value = "${aws_alb.application_loadbalancer.name}"
}

output "alb_dns" {
  value = "${aws_alb.application_loadbalancer.dns_name}"
}

output "target_group_arns" {
  value = ["${aws_alb_target_group.alb_target_group.*.arn}"]
}

