
# define Autoscalling-group
resource "aws_launch_configuration" "launch_configuration" {
  associate_public_ip_address = "${var.associate_public_ip}"
  image_id                    = "${var.ami_id}"
  iam_instance_profile        = "${var.iam_instance_profile}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.ssh_key_name}"
  security_groups             = ["${var.security_groups_autoscalling_ids}"]
  user_data                   = "${var.launch_script}"
}

resource "aws_autoscaling_group" "autoscaling_group" {
  health_check_type    = "${var.health_check_type}"
  launch_configuration = "${aws_launch_configuration.launch_configuration.id}"
  load_balancers       = ["${var.classic_loadbalancer_ids}"]
  max_size             = "${var.autoscaling_group_max_size}"
  min_size             = "${var.autoscaling_group_min_size}"
  target_group_arns = ["${var.application_loadbalncer_target_groups}"]

  vpc_zone_identifier = ["${var.subnet_ids}"]

  tag {
    key                 = "Name"
    value               = "webserver-masterthesis-2017"
    propagate_at_launch = true
  }

  # needs to be specified as inner resource to make sure it is executed on each instance
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_cloudwatch_metric_alarm" "high_cpu_utilization" {
  alarm_name = "Instance-high-cpu-utilization"
  namespace = "AWS/EC2"
  metric_name = "CPUUtilization"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.autoscaling_group.name}"
  }
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods = 1
  period = 300
  statistic = "Average"
  threshold = 90
  unit = "Percent"
}