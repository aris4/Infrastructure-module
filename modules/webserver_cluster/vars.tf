# Variable für Loadbalancer
variable ports {
  description = "map of ports & protocols to be added as listeners to this application loadbalancer. defaults to HTTP listener on port 8080"
  type        = "map"

  default = {
    "80" = "HTTP"
  }
}

variable protocol {
  default = "HTTP"
}

variable resource_default_tags {
  description = "map: tags that will be set on all resources that support tagging"
  type        = "map"

  default = {
    module = "webserver_cluster"
  }
}


variable subnet_ids {
  description = "list of IDs of subnets to be served by this load balancer. no default provided"
  type        = "list"
}

# Variable Autoscalling
variable ami_id {
  description = "the ID of an amazon machine image to be used for instances within this auto scaling group. no default provided"
  type        = "string"
}

variable application_loadbalncer_target_groups {
  description = "a list of ARNs of target groups in case an application load balancer is used. default is an empty list"
  type        = "list"
  default     = []
}

variable associate_public_ip {
  description = "if set to true, the instance will get a public IP address. defaults to 'false'"
  type        = "string"
  default     = "false"
}

variable autoscaling_group_max_size {
  description = "maximum number of instances for this auto scaling group. default is 1"
  type        = "string"
  default     = 1
}

variable autoscaling_group_min_size {
  description = "minimum number of instances for this auto scaling group. default is 1"
  type        = "string"
  default     = 1
}

variable classic_loadbalancer_ids {
  description = "list of IDs of classic loadbalancers that serve traffic to instances in this auto scaling group. default is an empty list"
  type        = "list"
  default     = []
}

variable health_check_type {
  description = "sets the type of the health check to be performed by the auto scaling group. valid values are 'ELB' (default) or 'EC2"
  type        = "string"
  default     = "ELB"
}

variable iam_instance_profile {
  description = "name of an IAM role to be set as instance profile for each ec2 instance in this auto scaling group. empty by default"
  type        = "string"
  default     = ""
}

variable instance_type {
  description = "the amazon instance type ID for ec2 instances in this auto scaling group"
  type        = "string"
}

variable launch_script {
  description = "define an inline script that is executed when an instance launchs via the user data form here. empty by default"
  type        = "string"
  default     = ""
}

variable security_groups_autoscalling_ids {
  description = "list of security groups that will be attached to ec2 instances in this auto scaling group. no default provided"
  type        = "list"
}

variable ssh_key_name {
  description = "the name of a ssh key pair generated in the AWS region that can connect to the instances in this auto scaling group. instances will be created without keys by default"
  type        = "string"
}


variable aws_region {
  description = "id of region to create resources in; default is eu-central-1 (Frankfurt)"
}