# Outputs Autoscalling
output arn {
  value = "${aws_autoscaling_group.autoscaling_group.arn}"
}

output id {
  value = "${aws_autoscaling_group.autoscaling_group.id}"
}

output name {
  value = "${aws_autoscaling_group.autoscaling_group.name}"
}

