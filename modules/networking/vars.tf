variable aws_region {
  description = "id of region to create resources in; default is eu-central-1 (Frankfurt)"
}

variable az_appendices {
  description = "list: availability zones to create resources in; default is a & b"
  type        = "list"
  default     = ["a","b"]
}

variable create_nat_gateway {
  description = "flag that indicated wheter or not a nat gateway for each public subnet should be created. defaults to 'false'"
  type        = "string"
  default     = "false"
}

variable ip_range_start {
  description = "the VPC's base IP range. defaults to '10.0.0.0'."
  type        = "string"
  default     = "10.0.0.0"
}

variable resource_default_tags {
  description = "map: tags that will be set on all resources that support tagging"
  type        = "map"

  default = {
    module = "networking"
  }
}
