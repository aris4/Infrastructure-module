# create a single VPC for all resources
resource "aws_vpc" "env-vpc" {
  cidr_block = "${var.ip_range_start}/16"
  tags       = "${var.resource_default_tags}"
}

# create an internet gateway so that resources within the VPC can connect to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.env-vpc.id}"
  tags   = "${var.resource_default_tags}"
}

# create a public subnet
resource "aws_subnet" "public-subnet" {
  count             = "${length(var.az_appendices)}"
  availability_zone = "${var.aws_region}${element(var.az_appendices, count.index)}"
  vpc_id            = "${aws_vpc.env-vpc.id}"
  cidr_block        = "${cidrhost("${var.ip_range_start}/16", count.index * 256 )}/24"
  tags              = "${var.resource_default_tags}"
}

# create routing tables for subnets

## route tables
resource "aws_route_table" "rt-public-subnet" {
  vpc_id = "${aws_vpc.env-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }
}

## associate route tables with subnets -route
resource "aws_route_table_association" "rt-public-subnet" {
  count          = "${aws_subnet.public-subnet.count}"
  subnet_id      = "${element(aws_subnet.public-subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.rt-public-subnet.id}"
}


