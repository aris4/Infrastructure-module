
#define security-groups

resource "aws_security_group" "security_group" {
  vpc_id = "${var.vpc_id}"
  tags   = "${var.resource_default_tags}"
}

resource "aws_security_group_rule" "cidr_source_egress_rules" {
  count             = "${length(var.cidr_source_egress_rules)}"
  cidr_blocks       = ["${split(",", lookup(var.cidr_source_egress_rules,
                        element(keys(var.cidr_source_egress_rules), count.index)))}"]
  from_port         = "${element(split(".", element(keys(var.cidr_source_egress_rules), count.index)), 0)}"
  to_port           = "${element(split(".", element(keys(var.cidr_source_egress_rules), count.index)), 0)}"
  protocol          = "${element(split(".", element(keys(var.cidr_source_egress_rules), count.index)), 1)}"
  security_group_id = "${aws_security_group.security_group.id}"
  type              = "egress"
}

resource "aws_security_group_rule" "cidr_source_ingress_rules" {
  count             = "${length(var.cidr_source_ingress_rules)}"
  cidr_blocks       = ["${split(",", lookup(var.cidr_source_ingress_rules, element(keys(var.cidr_source_ingress_rules)
  , count.index)))}"]
  from_port         = "${element(split(".", element(keys(var.cidr_source_ingress_rules), count.index)), 0)}"
  to_port           = "${element(split(".", element(keys(var.cidr_source_ingress_rules), count.index)), 0)}"
  protocol          = "${element(split(".", element(keys(var.cidr_source_ingress_rules), count.index)), 1)}"
  security_group_id = "${aws_security_group.security_group.id}"
  type              = "ingress"
}

resource "aws_security_group_rule" "sgid_source_ingress_rules" {
  count                    = "${length(var.sg_source_ingress_rules)}"
  from_port                = "${element(split(".", element(keys(var.sg_source_ingress_rules), count.index)), 0)}"
  to_port                  = "${element(split(".", element(keys(var.sg_source_ingress_rules), count.index)), 0)}"
  protocol                 = "${element(split(".", element(keys(var.sg_source_ingress_rules), count.index)), 1)}"
  source_security_group_id = "${lookup(var.sg_source_ingress_rules, element(keys(var.sg_source_ingress_rules), count.index))}"
  security_group_id        = "${aws_security_group.security_group.id}"
  type                     = "ingress"
}
