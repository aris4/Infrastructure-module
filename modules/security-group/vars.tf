
variable cidr_source_egress_rules {
  description = "map of ports & protocols to be opened as well as their configurations. all outgoing traffic is allowed by default."
  type        = "map"

  default = {
    "0.all" = "0.0.0.0/0"
  }
}

variable cidr_source_ingress_rules {
  description = "map of ports & protocols to be opened as well as their configurations. web traffic (tcp port 80 & 443) are open for the Capgemini network by default."
  type        = "map"

  default = {
    "80.tcp"  = "0.0.0.0/0"
  }
}

variable resource_default_tags {
  description = "map: tags that will be set on all resources that support tagging"
  type        = "map"

  default = {
    module = "security-group"
  }
}

variable sg_source_ingress_rules {
  description = "map of ports & protocols to be opened as well as their configurations. this map is empty by default."
  type        = "map"

  default = {
    # "1234.udp" = "sg-12345"
  }
}

variable vpc_id {
  description = "id vpc. default: empty"
  type        = "string"
  default     = ""
}

variable aws_region {
}