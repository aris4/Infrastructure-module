variable "rds_identifier" {
  default = "db"
}

variable "rds_instance_type" {
  description = "instance type of the database engine possible option 1. micro 2.medium 3. large"
}

variable "rds_storage_size" {
  description = "the size of the rds storage"
}

variable "rds_engine" {
}

variable "rds_engine_version" {
  default = "9.5.2"
}

variable "rds_db_name" {
  default = "masterarbeit-database"
}

variable "rds_admin_user" {
}

variable "rds_admin_password" {
}

variable "rds_publicly_accessible" {
  default = "true"
}

variable "database_port" {
  default = "5432"
}

variable "subnet_ids" {
  type = "list"
}

variable "aws_region" {
  description = "aws region to deploy the rds"
}

variable "rds_instance_identifier" {
  description = "Custom name of the instance"
  default = ""
}

variable "vpc_security_group_ids" {
  description = "list of security groups that will be attached to RDS instances"
  type        = "list"
}

variable resource_default_tags {
  type = "map"

  default = {
    application       = "testumgebung"
    owner             = "aristide.de@capgemini.com"
    project           = "masterarbeit"
    stage             = "test"
    terraform_managed = "true"
  }
}
