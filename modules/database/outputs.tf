output "database-adress" {
  value = "address: ${aws_db_instance.db.address}"
}

output "database-name" {
  value = "${aws_db_instance.db.name}"
}